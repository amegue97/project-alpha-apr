from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm


def sign_up(request):
    # If the form is submitted
    if request.method == "POST":
        new_user_form = UserCreationForm(request.POST)
        if new_user_form.is_valid():
            new_user_form.save()
            return redirect("home")
    # If the page is loaded
    else:
        new_user_form = UserCreationForm()

    context = {"form": new_user_form}
    return render(request, "registration/signup.html", context)
